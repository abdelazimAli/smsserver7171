var express = require('express');
var router = express.Router();
var { utilities } = require('../utilities');
router.get('/', function(req, res, next) {
    var msisdn = req.query.msisdn;
    var msg = req.query.message;
    let valid = utilities.validateMsisdn(msisdn);
    
    if (!valid) {
        res.status(400).send({error:"Invalid Number"});
    }
    console.log(`${new Date().toString()}:  message:${msg} To:${msisdn} `);
    res.status(200).send({
        msisdn,
        msg,
        status:1
    });


  });
  

  router.post('/', function(req, res, next) {
    var msisdn = req.body.msisdn;
    var msg = req.body.message;
    let valid = utilities.validateMsisdn(msisdn);
    
    if (!valid) {
        res.status(400).send({error:"Invalid Number"})
    }else{
        console.log(`message:${msg} To:${msisdn} `);
        res.status(200).send({
            msisdn,
            msg,
            status:1
        });
    }



  });

  module.exports = router;